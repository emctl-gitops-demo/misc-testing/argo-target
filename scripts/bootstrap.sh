NAME=$(kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2)
echo "waiting for pod $NAME"

while [[ $(kubectl get pods $NAME -n argocd -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; 
  do 
     sleep 60;
  done