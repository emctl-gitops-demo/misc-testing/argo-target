#SHELL = /home/linuxbrew/.linuxbrew/bin//zsh

create:
	k3d cluster create -a 3 -p "80:80@loadbalancer" --k3s-server-arg '--no-deploy=traefik' --k3s-server-arg 'servicelb'
	kubectl create namespace argocd
	helm repo add argo https://argoproj.github.io/argo-helm
	helm repo update
	export ARGOCD_EXEC_TIMEOUT=300
	helm install gitops argo/argo-cd -n argocd -f argo-values.yaml
	scripts/bootstrap.sh
	kubectl port-forward service/gitops-argocd-server -n argocd 8080:80 &

delete:
	k3d cluster delete

reset: delete create 