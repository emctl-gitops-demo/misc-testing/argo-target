[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/emctl-gitops-demo/misc-testing/argo-target)

# ArgoCD-test

The purpose of this project is to follow the App Of Apps pattern for ArgoCD in order to bootstrap a k8 cluster.

Leveraging the ArgoCD concept of [App of Apps](https://argoproj.github.io/argo-cd/operator-manual/declarative-setup/#app-of-apps),
you will be able to install a number of kubernetes manifests quickly, safely and repeatedly.


The scripts folder, argo-values.yaml and Makefile are only for the use of k3d and you can easily tweak this for KinD

Please see my [website](https://emctl.gitlab.io/hugo/post/) for noteworthy updates
